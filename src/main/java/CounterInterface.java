/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Giuseppe
 */
public interface CounterInterface {
    
    public void increase();
    
    public void decrease();
    
    public int getValue();
}
