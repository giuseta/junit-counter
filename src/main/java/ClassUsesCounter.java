/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Giuseppe
 */
public class ClassUsesCounter {
    
    private CounterInterface counter;
    
    public ClassUsesCounter(CounterInterface c) {
        counter = c;
    }
    
    public int multiplyCounterValue(int multiplier) {
        return counter.getValue() * multiplier;
    }
    
}
