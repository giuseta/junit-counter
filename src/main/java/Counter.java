/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Giuseppe
 */
public class Counter implements CounterInterface {

    private int value;

    public Counter(int value) {
        this.value = value;
    }

    public void increase() {
        value++;
    }

    public void decrease() {
        if (value > 0) {
            value--;
        }
    }

    public int getValue() {
        return value;
    }

}
